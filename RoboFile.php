<?php

use Robo\Collection\CollectionBuilder;
use Robo\Symfony\ConsoleIO;

/**
 * Robo tasks for Ramsalt Media 2.0
 *
 * @see https://robo.li/
 */
class RoboFile extends \Robo\Tasks {

  /**
   * The URL to the demo repository to fetch files from.
   */
  const DEMO_REPOSITORY_URL = 'git@bitbucket.org:ramsalt/ramsaltmedia-demo.ramsalt.com-composer';

  /**
   * Copy over the rm2_demo theme from the demo to the current project.
   *
   * Command: vendor/bin/robo copy:demo-theme-from-demo
   *
   */
  public function copyDemoThemeFromDemo(ConsoleIO $io) {
    $collection = $this->collectionBuilder($io);
    return $this->copyDirectoryFromRepository($collection, self::DEMO_REPOSITORY_URL, 'web/themes/custom/rm2_demo');
  }

  /**
   * Copy over the Drupal config folder from the demo to the current project.
   *
   * Command: vendor/bin/robo copy:config-from-demo
   *
   */
  public function copyConfigFromDemo(ConsoleIO $io) {
    $collection = $this->collectionBuilder($io);
    return $this->copyDirectoryFromRepository($collection, self::DEMO_REPOSITORY_URL, 'config/sync');
  }

  /**
   * Copy a directory from a GIT repository into the current project.
   *
   * It will use the same path as in the GIT repository.
   *
   * @param \Robo\Collection\CollectionBuilder $collection
   *   The robo collection.
   * @param string $repositoryUrl
   *   The repository URL.
   * @param string $directory
   *   The directory to copy over.
   *
   * @return \Robo\Result
   *   The Robo task result.
   */
  protected function copyDirectoryFromRepository(CollectionBuilder $collection, string $repositoryUrl, string $directory) {
    $tmpDirectory = $collection->tmpDir();

    // Clone the repository in a temporary directory.
    $collection->taskGitStack()
      ->stopOnFail()
      ->cloneRepo($repositoryUrl, $tmpDirectory, 'master');
    $from = $tmpDirectory . '/' .$directory;

    // Copy the desired folder into the current project.
    $collection->taskCopyDir([$from => $directory]);
    return $collection->run();
  }

}
