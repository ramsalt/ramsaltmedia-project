# Composer template for RamsaltMedia 2 projects

RamsaltMedia is based on the
[https://github.com/thunder/thunder-distribution](Thunder distribution) and is
basically maintained by the
[Ramsalt Media Metapackage](https://bitbucket.org/ramsalt/ramsaltmedia-meta/src)
which handles all module dependencies.

## Project creation

You can create a new RamsaltMedia project with the following command.
`composer create-project ramsalt/ramsaltmedia-project:dev-master some-dir --no-interaction`

A full installation guide you can find at
https://kb-ramsalt.atlassian.net/wiki/spaces/RM2/pages/1720025089/Install+a+fresh+Ramsalt+Media+2.0+site

## Update RamsaltMedia codebase

### 1) Before the Update
Make sure that the synced configuration in your project repository is up to
date with the production database. Otherwise you might end up with undesired
results.

### 2a) Update RamsaltMedia codebase
To update the RamsaltMedia codebase use the following composer command.
`composer update "drupal/core*" thunder/thunder-distribution ramsalt/ramsaltmedia-meta -W`.

### 2b) Upgrade RamsaltMedia codebase
RamsaltMedia tags releases using semantic versioning. This composer template
installs RamsaltMedia with the recent major version, e.g. `^5.0`.
If there is a new major version released, you need to bump this version to e.g.
`^7.0`.

### 3) Update RamsaltMedia database
After each update of the RamsaltMedia codebase you should run `drush updb`.

### 4) Export the configuration
Export the configuration with `drush cex`.

### 5) Deploy the changes to the server.
Deploy the changes to the server and run `drush updb` also there. This is
required because module updates might also perform non-configuration related
changes. After that `drush cs` should result in a clean configuration.
